#!/usr/bin/env python3

# These sizing mechanisms do not seem to work for block devices on mac
def blockdev_size(path):
    """Return device size in bytes.
    """
    with open(path, 'rb') as f:
        return f.seek(0, 2) or f.tell()

def get_file_size(filename):
    "Get the file size by seeking at end"
    import os
    fd= os.open(filename, os.O_RDONLY)
    try:
        return os.lseek(fd, 0, os.SEEK_END)
    finally:
        os.close(fd)


import math
def Entropy(string,base = 2.0):
    # Get all individual symbols used (Should we instead get a list of all symbols?)
    symbols = dict.fromkeys(list(string))

    # Calculate symbol probabilities
    prob =  [float(string.count(sym)) / len(string) for sym in symbols]

    # Calculate Entropy
    H = -sum([p  * math.log(p) / math.log(base) for p in prob ])
    return H

import lzma
def CompressRatio(data):
    compressed = lzma.compress(data)

    ratio = len(data)/len(compressed)
    return ratio

def main():
    import argparse

    parser = argparse.ArgumentParser(
        description="Make a map of the entropy or compressibility of the device"
    )

    parser.add_argument(
        "-v", "--version", action="version",
        version = f"{parser.prog} version 0.0.1"
    )
    parser.add_argument('device',
        help="The device (or file) of which to draw the map"
    )
    parser.add_argument("-w", "--imageWidth", type=int, default="1024",
        help="The width of the image, in pixels, to be produced"
    )
    parser.add_argument("-b", "--blocksize", type=int, default="512",
        help="The size, in bytes, of the blocks to be read from the device, each block corresponds to one pixel in the image"
    )
    parser.add_argument("-c", "--useCompress", action="store_true",
        help="By default, the entropy of the blocks will be calculated and drawn, if this option is passed, the compressibility of the blocks will be used instead"
    )
    parser.add_argument("-a", "--useViridis", action="store_true",
        help="Use the Viridis colormap instead of the Plasma one used by default"
    )
    parser.add_argument("-l", "--limitRows", action="store_true",
        help="Limit the image rows, by default as many rows as necessary to represent the whole device will be used, if this option is passed, only 300 rows will be drawn"
    )

    args = parser.parse_args()

    import os
    import stat
    import numpy as np
    import matplotlib.pyplot as plt

    isGood = False
    stat_res = os.stat(args.device)
    mode = stat_res.st_mode
    if stat.S_ISBLK(mode):
        print("{} is a block device".format(args.device))
        isGood = True
    if stat.S_ISCHR(mode):
        print("{} is a special character device".format(args.device))
        isGood = True
    if stat.S_ISREG(mode):
        print("{} is a regular file".format(args.device))
        print("Size info: {}".format(stat_res.st_size))
        isGood = True
    if stat.S_ISSOCK(mode):
        print("{} is a socket".format(args.device))
    if not isGood:
        print("You must supply a block device, special character device or regular file. Exiting")
        return 1

    blockSize = args.blocksize
    base = 2
    colormap = "plasma"
    if args.useViridis:
        colormap = "viridis"

    min = 0
    max = math.log(256)/math.log(base)
    if args.useCompress:
        minSize = 26.874 * blockSize**0.1518
        maxSize = blockSize

        min = minSize/blockSize
        max = maxSize/blockSize

    print("min: {}; max: {}".format(min, max))

    row = 0
    col = 0
    maxCol = args.imageWidth
    imageData = []
    rowData = [None] * maxCol
    imageData.append(rowData)
    print("Creating row {}".format(row))
    with open(args.device, 'rb') as device:
        block = device.read(blockSize)
        while block:
            value = None
            if args.useCompress:
                value = 1/CompressRatio(block)
            else:
                value = Entropy(block, base)

            imageData[row][col] = value
            col += 1

            if col >= maxCol:
                row += 1
                col = 0
                rowData = [None] * maxCol
                imageData.append(rowData)
                if row%10 == 0:
                    print("Creating row {}".format(row))

            block = device.read(blockSize)

            if args.limitRows and row == 300:
                break

        imageData = np.array(imageData, dtype=np.float64)

        plt.imsave("map.png", imageData, cmap=colormap, vmin=min, vmax=max)
        plt.imsave("map.pdf", imageData, cmap=colormap, vmin=min, vmax=max)

        # Old approach, had issues because interpolation
        #fig = plt.figure(frameon=False)
        #im1 = plt.imshow(imageData, vmin = min, vmax = max, cmap=colormap)

        #fig.savefig('map.png', bbox_inches='tight')
        #fig.savefig('map.pdf', bbox_inches='tight')
        #plt.show()

if __name__ == "__main__":
    main()
