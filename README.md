# Device Mapper

A short script that makes images of the compressibility/entropy of sectors of a device.

The sector size is configurable as well as the width of the image (height is defined by
the device size, block size and image width).
This can be useful to monitor the data on the disk, used sectors, etc.

The output image will be saved as map.png and map.pdf in the current directory.

Below an example on how to run this script:
`python3 mapper.py -l /dev/disk1�`

The -l option will limit the output to 300 rows.
